FROM node:15-alpine as build 
WORKDIR /my-app
COPY my-app/ ./
RUN npm install && npm run build

FROM nginx:1.19.0
COPY --from=build /my-app/build /usr/share/nginx/html
